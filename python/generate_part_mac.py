#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 17:54:56 2017

@author: jmeidam
"""

import numpy as np
from subprocess import Popen, PIPE
from os import path, environ, makedirs
import random
import string

'''
The height of the image is width * aspectratio
'''

environ["LDRAWDIR"] = path.join(environ["HOME"], "lego", "parts", "ldraw")

l3p = path.join(environ["HOME"], "lego", "software", "l3p")
pov = path.join(environ["HOME"], "lego", "software", "PovrayCommandLineMacV2", "Povray37UnofficialMacCmd")
workdir = path.join(environ["HOME"], "lego")
np.random.seed(42)

allpartcolors = list(range(0, 15+1)) + list(range(17, 23))\
    + list(range(25, 28+1)) + list(range(32, 47+1))\
    + [57] + list(range(256, 511+1))

selectpartcolors = [0, 1, 4, 15]

twobynbricks = ['3004', '3003', '3002', '3001', '2456', '3007', '3006']


def EnsureDir(f, stripfilename=False):
    """
    Create folder if it does not exist

    stripfilename : provides possibility to pass a path including a filename
                    if set to True
    """

    if stripfilename:
        F = path.dirname(f)
    else:
        F = f
    if not path.exists(F):
        makedirs(F)


def phitheta_to_xyz(phi, theta):
    """
    Calculate weird left-handed pov-ray x,y,z
    """
    y = -np.cos(theta)
    x = np.sin(theta) * np.cos(phi)
    z = -np.sin(theta) * np.sin(phi)
    return x, y, z


def render(brickid, aspectratio=1.0, width=0, color=1, backgroundcolor='White',
           outputdir=path.join(workdir, "renderedbricks"), verbose=False,
           jpeg=False, camera_angle=37, camera_roll=0,
           theta=0.0, phi=-np.pi/2.0, filename_extra=""):
    """
    render a single brick with id=brickid
    """

    if width == 0:
        width = 100
    height = width*aspectratio

    ext = "png"
    if jpeg:
        ext = "jpg"

    x, y, z = phitheta_to_xyz(theta, phi)

    # print("creating %s" % temppov)
    if filename_extra == "":
        outputimage = path.join(outputdir, "%s.%s" % (brickid, ext))
        temppov = path.join(workdir, "%s.pov" % brickid)
    else:
        outputimage = path.join(outputdir, "%s_%s.%s" % (brickid,
                                                         filename_extra, ext))
        temppov = path.join(workdir, "%s_%s.pov" % (brickid, filename_extra))

    command_l3p = l3p + \
        " %s -b%s -ca%.2f -cra%.2f -car%.2f -c%d -q3 -o %s -bu" % \
        (brickid, backgroundcolor, camera_angle, camera_roll, aspectratio,
         color, temppov)
    command_l3p += " -csky%.2f,%.2f,%.2f" % (x, y, z)
    command_pov = pov + " +I%s +O%s +W%d +H%d +Q9 +A +R2" % \
        (temppov, outputimage, width, height)
    if jpeg:
        command_pov += " +FJ"

    # print("removing %s" % temppov)
    pdel = Popen("rm %s" % temppov, stdout=PIPE, stderr=PIPE, shell=True)

    p1 = Popen(command_l3p, stdout=PIPE, stderr=PIPE, shell=True)
    p2 = Popen(command_pov, stdout=PIPE, stderr=PIPE, shell=True)

    if verbose:
        print(command_l3p)
        print(command_pov)

    return p1, p2, pdel


def multirender_brick(brickids, N=100, outdir="", colors=[1],
                      ar=1.0, width=100, bc='White', jpeg=False,
                      ca=37, cr=0, settype=''):

    theta = np.random.uniform(low=-0.5, high=0.5, size=N) * np.pi
    phi = np.random.uniform(low=0.0, high=2.0, size=N) * np.pi
    colidx = np.random.randint(0, high=len(colors), size=N)

    if not isinstance(brickids, list):
        list(brickids)

    if settype != "":
        print("generating files for %s set..." % settype)
    for ID in brickids:
        print("  generating %d bricks with ID %s" % (N, ID))
        od = outdir
        if settype != "":
            od = path.join(workdir, "renderedbricks", settype, "%s" % ID)
        else:
            if outdir == "":
                od = path.join(workdir, "renderedbricks", settype, "%s" % ID)

        EnsureDir(od)

        for i in range(N):
            _, _, _ = render(ID, aspectratio=ar, width=width,
                             color=colors[colidx[i]],
                             backgroundcolor=bc, outputdir=od, verbose=False,
                             jpeg=jpeg, camera_angle=ca, camera_roll=cr,
                             theta=theta[i], phi=phi[i],
                             filename_extra="t%.2f_p%.2f" % (theta[i], phi[i]))
