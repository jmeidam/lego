# README #

## Don't expect too much here

This is just a repository to save my "messin' about" with automatically generating images of parts using l3p and pov-ray on Mac.
There are also Jupyter notebooks where I was in the early stage of experimenting with neural networks and image recognition. 
I tried to build a network that could recognise bricks from almost nay angle. I knew this was ambitious, the goal was mostly to learn about
image recognition and play around with Lego while I'm at it.

## How do I get set up?

At this moment the image generation from parts is only implemented for Mac. 
This repository is perhaps a nice example to how to get this to work on a Mac for those who are interested.
The neural network python scripts are not OS specific I believe, I haven't checked this yet.

To get the required 3rd party software Pov-Ray and l3p for mac I included a bash script that installs 
these inside the repository's working directory.

### L3P

You can find information on L3P on this website: http://www.hassings.dk/l3/l3p.html
The download link I used in the script can be found near the bottom of that page under
the Downloads section.

### Pov-Ray for Mac

You can find information about this software on http://megapov.inetart.net/povrayunofficial_mac/index.html#Mac.
The download link I used in the script can be found on the right. I chose the precompiled commandline only utility.

### LDraw parts list

Of course you will need the LDraw parts list to render anything. This can be found on ldraw.org under parts.
The zip containing all the parts can be downloaded via this link: http://www.ldraw.org/library/updates/complete.zip

## Notes

 - This repository is not supposed to be a neat module. It's just a gathering of experiments and scripts.