ZIP_POVRAY="PovrayCommandLineMacV2.zip"
URL_POVRAY="http://megapov.inetart.net/povrayunofficial_mac/povray_downloads/${ZIP_POVRAY}"

ZIP_L3P="l3p14betamacosx.zip"
URL_L3P="http://www.hassings.dk/l3/l3p/${ZIP_L3P}"

curl -O ${URL_POVRAY}
unzip ${ZIP_POVRAY}
# no idea why this is here. Seems to work fine without
rm -r __MACOSX
rm ${ZIP_POVRAY}

curl -O ${URL_L3P}
unzip ${ZIP_L3P}
rm ${ZIP_L3P}

